﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task04
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculation(35, 25);
        }
        static void Calculation(int a, int b)
        {
            Console.WriteLine($"{a} + {b} = {a + b}");
            Console.WriteLine($"{a} - {b} = {a - b}");
            Console.WriteLine($"{a} / {b} = {a / b}");
            Console.WriteLine($"{a} * {b} = {a * b}");
        }
    }
}
